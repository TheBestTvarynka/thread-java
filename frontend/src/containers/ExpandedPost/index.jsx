import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleExpandedUpdatePost,
  loadCommentReactors,
  loadPostReactors,
  addComment,
  likeComment,
  dislikeComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  curUserId,
  loadPostReactors: loadPReactors,
  loadCommentReactors: loadCReactors,
  likePost: likeP,
  dislikePost: dislikeP,
  toggleExpandedPost: toggle,
  toggleExpandedUpdatePost: toggleUpdate,
  addComment: add,
  likeComment: likeC,
  dislikeComment: dislikeC
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={likeP}
            dislikePost={dislikeP}
            toggleExpandedPost={toggle}
            toggleExpandedUpdatePost={toggleUpdate}
            sharePost={sharePost}
            curUserId={curUserId}
            loadReactors={loadPReactors}
          />
          <CommentUI.Group>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  curUserId={curUserId}
                  postId={post.id}
                  loadReactors={loadCReactors}
                  updateComment={add}
                  likeComment={likeC}
                  dislikeComment={dislikeC}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  curUserId: PropTypes.string.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired,
  loadCommentReactors: PropTypes.func.isRequired,
  loadPostReactors: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  loadCommentReactors,
  loadPostReactors,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleExpandedUpdatePost,
  addComment,
  likeComment,
  dislikeComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
