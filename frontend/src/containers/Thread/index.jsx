import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import { Dropdown, Loader } from 'semantic-ui-react';
import SharedPostLink from 'src/components/SharedPostLink';
import AddPost from 'src/components/AddPost';
import * as imageService from 'src/services/imageService';
import Post from 'src/components/Post';
import ExpandedPost from 'src/containers/ExpandedPost';
import UpdatePost from '../../components/UpdatePost';
import {
  loadPostReactors,
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleExpandedUpdatePost,
  addPost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  filterType: 'ALL_POSTS',
  param: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPostReactors: reactors,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  expandedUpdatePost,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  toggleExpandedUpdatePost: toggleUpdate
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);

  const toggleFilterPosts = (type, param) => {
    // filtering
    postsFilter.filterType = type;
    postsFilter.param = param;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Dropdown
          text="Filter Posts"
          icon="filter"
          floating
          labeled
          button
          className="icon"
        >
          <Dropdown.Menu>
            <Dropdown.Item text="All posts" onClick={() => toggleFilterPosts('ALL_POSTS', undefined)} />
            <Dropdown.Item text="Own posts" onClick={() => toggleFilterPosts('OWN_POSTS', userId)} />
            <Dropdown.Item text="Foreign posts" onClick={() => toggleFilterPosts('FOREIGN_POSTS', userId)} />
            <Dropdown.Item text="Liked posts" onClick={() => toggleFilterPosts('LIKED_POSTS', userId)} />
          </Dropdown.Menu>
        </Dropdown>
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            loadReactors={reactors}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            toggleExpandedUpdatePost={toggleUpdate}
            sharePost={sharePost}
            curUserId={userId}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} curUserId={userId} />}
      {expandedUpdatePost && <UpdatePost />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  expandedUpdatePost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadPostReactors: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  expandedUpdatePost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  expandedUpdatePost: rootState.posts.expandedUpdatePost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPostReactors,
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleExpandedUpdatePost,
  addPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
