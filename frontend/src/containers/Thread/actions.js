import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EXPANDED_UPDATE_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setExpandedUpdatePostAction = post => ({
  type: SET_EXPANDED_UPDATE_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const deletePost = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.filter(post => (post.id !== postId));
  dispatch(setPostsAction(updated));
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(undefined));
  }
};

export const updatePost = updatedPost => async (dispatch, getRootState) => {
  await postService.addPost(updatedPost);
  const newPost = await postService.getPost(updatedPost.id);

  const mapUpdates = post => ({
    ...post,
    body: newPost.body,
    image: newPost.image
  });

  const { posts: { posts, expandedUpdatePost, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== updatedPost.id
    ? post
    : mapUpdates(post)));

  dispatch(setPostsAction(updated));

  if (expandedUpdatePost && expandedUpdatePost.id === updatedPost.id) {
    dispatch(setExpandedUpdatePostAction(mapUpdates(expandedUpdatePost)));
  }
  if (expandedPost && expandedPost.id === updatedPost.id) {
    dispatch(setExpandedPostAction(mapUpdates(expandedPost)));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const toggleExpandedUpdatePost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedUpdatePostAction(post));
};

const updatePostReactions = (dispatch, getRootState, postId, diffLike, diffDislike) => {
  const mapReaction = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diffLike, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + diffDislike // diff is taken from the current closure
  });
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReaction(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReaction(expandedPost)));
  }
};

export const likePost = postId => async (dispatch, getRootState) => {
  const result = await postService.likePost(postId);
  const diffLike = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffDislike = ((!result || result.isNew) ? 0 : -1);
  updatePostReactions(dispatch, getRootState, postId, diffLike, diffDislike);
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const result = await postService.dislikePost(postId);
  const diffDislike = result?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - like was removed
  const diffLike = ((!result || result.isNew) ? 0 : -1);
  updatePostReactions(dispatch, getRootState, postId, diffLike, diffDislike);
};

const getNewComments = (id, oldComments, newComment) => {
  if (id) {
    return oldComments ? oldComments.map(comment => (comment.id === newComment.id ? newComment : comment)) : [];
  }
  return [...(oldComments || []), newComment];
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + (request.id ? 0 : 1),
    comments: getNewComments(request.id, post.comments, comment)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

const deleteCommentFromPost = (commentId, postId, post) => {
  if (post.comments) {
    return post.comments.filter(comment => (comment.id !== commentId));
  }
  return [];
};

export const deleteComment = (commentId, postId) => async (dispatch, getRootState) => {
  await commentService.deleteComment(commentId);
  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: deleteCommentFromPost(commentId, postId, post)
  }
  );

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id === postId
    ? mapComments(post)
    : post));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

const updateCommentReactions = (dispatch, getRootState, commentId, diffLike, diffDislike) => {
  const mapReaction = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diffLike,
    dislikeCount: Number(comment.dislikeCount) + diffDislike
  });
  const { posts: { expandedPost } } = getRootState();
  const updatedExpandedPost = {
    ...expandedPost,
    comments: expandedPost.comments.map(comment => (comment.id === commentId
      ? mapReaction(comment)
      : comment
    ))
  };
  dispatch(setExpandedPostAction(updatedExpandedPost));
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.likeComment(commentId);
  const diffLike = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const diffDislike = ((!result || result.isNew) ? 0 : -1);
  updateCommentReactions(dispatch, getRootState, commentId, diffLike, diffDislike);
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.dislikeComment(commentId);
  const diffDislike = result?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - like was removed
  const diffLike = ((!result || result.isNew) ? 0 : -1);
  updateCommentReactions(dispatch, getRootState, commentId, diffLike, diffDislike);
};

export const loadPostReactors = (postId, isLiked) => async () => {
  const result = isLiked ? await postService.getPostLikers(postId) : await postService.getPostDislikers(postId);
  console.log(result);
  // dispatch(setUsers(result));
  return result;
};

export const loadCommentReactors = (commentId, isLiked) => async () => {
  const result = isLiked
    ? await commentService.getCommentLikers(commentId)
    : await commentService.getCommentDislikers(commentId);
  console.log(result);
  // dispatch(setUsers(result));
  return result;
};
