import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Button, Form, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import UserList from 'src/components/UsersList';
import DeleteComment from 'src/components/DeleteComment';

import styles from './styles.module.scss';

const Comment = ({
  comment: { body, createdAt, user, id, likeCount, dislikeCount },
  postId,
  curUserId,
  loadReactors,
  updateComment,
  likeComment,
  dislikeComment
}) => {
  const [edit, toggleEdit] = useState(false);
  const [text, setText] = useState(body);

  const cancelUpdate = () => {
    setText(body);
    toggleEdit(false);
  };

  const handleUpdateComment = async () => {
    if (!text) {
      return;
    }
    await updateComment({ id, postId, body: text });
    toggleEdit(false);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Metadata>
          <span className="right floated">
            {curUserId === user.id
            && (
              <CommentUI.Actions>
                <CommentUI.Action>
                  <div onClick={() => toggleEdit(true)}>
                    Update
                  </div>
                </CommentUI.Action>
                <CommentUI.Action>
                  <DeleteComment id={id} postId={postId} />
                </CommentUI.Action>
              </CommentUI.Actions>
            )}
          </span>
        </CommentUI.Metadata>
        {edit ? (
          <Form reply onSubmit={handleUpdateComment}>
            <Form.TextArea
              value={text}
              onChange={ev => setText(ev.target.value)}
            />
            <Button type="submit" content="Update comment" labelPosition="left" icon="edit" primary size="mini" />
            <Button
              content="Cancel"
              labelPosition="left"
              icon="close"
              onClick={cancelUpdate}
              secondary
              size="mini"
            />
          </Form>
        ) : (
          <CommentUI.Text>
            {text}
          </CommentUI.Text>
        )}
      </CommentUI.Content>
      <CommentUI.Actions>
        <UserList
          triggerTittle={likeCount.toString()}
          windowTitle="Users who likes your post"
          getter={() => loadReactors(id, true)}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id)}>
          <Icon name="thumbs up" />
        </Label>
        <UserList
          triggerTittle={dislikeCount.toString()}
          windowTitle="Users who dislikes your post"
          getter={() => loadReactors(id, false)}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id)}>
          <Icon name="thumbs down" />
        </Label>
      </CommentUI.Actions>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  curUserId: PropTypes.string.isRequired,
  loadReactors: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
