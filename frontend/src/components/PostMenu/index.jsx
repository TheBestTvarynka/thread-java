import React from 'react';
import PropTypes from 'prop-types';
import { Button, Dropdown } from 'semantic-ui-react';
import DeletePost from 'src/components/DeletePost';

// TODO: move all css code to separate file2

const PostMenu = ({ id, toggleExpandedUpdatePost }) => (
  <Dropdown
    className="button icon tiny"
    icon="sidebar"
    floating
    trigger={<></>}
  >
    <Dropdown.Menu>
      <Dropdown.Item>
        <Button
          style={{ backgroundColor: 'transparent', padding: '2px 5px 2px 5px' }}
          onClick={() => toggleExpandedUpdatePost(id)}
        >
          Update
        </Button>
      </Dropdown.Item>
      <Dropdown.Item>
        <DeletePost id={id} />
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

PostMenu.propTypes = {
  id: PropTypes.string.isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired
};

export default PostMenu;
