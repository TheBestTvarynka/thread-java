import React, { Component } from 'react';
import { Button, Icon, Modal, Header } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { deleteComment } from '../../containers/Thread/actions';

// TODO: rewrite this component with arrow function.

class DeleteComment extends Component {
  constructor(props) {
    super(props);
    this.state = { modalOpen: false, id: props.id };
    this.postId = props.postId;
    this.deleteComment = props.deleteComment;
  }

  handleOpen = () => this.setState({ modalOpen: true })

  handleClose = () => this.setState({ modalOpen: false })

  handleDeleteComment = async () => {
    const { id } = this.state;
    await this.deleteComment(id, this.postId);
    this.handleClose();
  };

  render() {
    const { modalOpen } = this.state;
    return (
      <Modal
        trigger={<div onClick={this.handleOpen}>Delete</div>}
        open={modalOpen}
        onClose={this.handleClose}
        size="small"
      >
        <Header icon="browser" content="Confirm an action" />
        <Modal.Content>
          <h3>You are sure you want to delete this comment?</h3>
        </Modal.Content>
        <Modal.Actions>
          <Button color="green" onClick={this.handleClose}>
            <Icon name="close" />
            Cancel
          </Button>
          <Button color="red" onClick={this.handleDeleteComment}>
            <Icon name="check" />
            Yes
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

DeleteComment.propTypes = {
  id: PropTypes.string.isRequired,
  postId: PropTypes.string.isRequired,
  deleteComment: PropTypes.func.isRequired
};

const actions = { deleteComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(DeleteComment);
