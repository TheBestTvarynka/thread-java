import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Label, Feed } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';

import styles from '../Post/styles.module.scss';

const UserList = ({ triggerTittle, windowTitle, getter }) => {
  const [curUsers, setCurUsers] = useState([]);

  const getUsers = async () => {
    const result = await getter();
    setCurUsers(result);
  };

  return (
    <Modal
      trigger={<Label basic size="mini" as="a" className={styles.toolbarBtn} content={triggerTittle} />}
      onOpen={getUsers}
    >
      <Modal.Header>{windowTitle}</Modal.Header>
      <Modal.Content>
        <Feed>
          {curUsers
            ? curUsers.map(user => (
              <Feed.Event key={user.username}>
                <Feed.Label image={user.image.link} />
                <Feed.Content>
                  <Feed.Summary>{user.username}</Feed.Summary>
                </Feed.Content>
              </Feed.Event>
            ))
            : <Spinner />}
        </Feed>
      </Modal.Content>
    </Modal>
  );
};

UserList.propTypes = {
  triggerTittle: PropTypes.string.isRequired,
  windowTitle: PropTypes.string.isRequired,
  getter: PropTypes.func.isRequired
};

export default UserList;
