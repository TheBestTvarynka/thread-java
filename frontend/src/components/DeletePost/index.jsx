import React, { Component } from 'react';
import { Button, Icon, Modal, Header } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { deletePost } from '../../containers/Thread/actions';

// TODO: rewrite this component with arrow function.

class DeletePost extends Component {
  constructor(props) {
    super(props);
    this.state = { modalOpen: false, id: props.id };
    this.deletePost = props.deletePost;
  }

  handleOpen = () => this.setState({ modalOpen: true })

  handleClose = () => this.setState({ modalOpen: false })

  handleDeletePost = async () => {
    const { id } = this.state;
    await this.deletePost(id);
  };

  render() {
    const { modalOpen } = this.state;
    return (
      <Modal
        trigger={(
          <Button
            style={{ backgroundColor: 'transparent', padding: '2px 5px 2px 5px' }}
            onClick={this.handleOpen}
          >
            Delete
          </Button>
        )}
        open={modalOpen}
        onClose={this.handleClose}
        size="small"
      >
        <Header icon="browser" content="Confirm an action" />
        <Modal.Content>
          <h3>You are sure you want to delete this post?</h3>
        </Modal.Content>
        <Modal.Actions>
          <Button color="green" onClick={this.handleClose}>
            <Icon name="close" />
            Cancel
          </Button>
          <Button color="red" onClick={this.handleDeletePost}>
            <Icon name="check" />
            Yes
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

DeletePost.propTypes = {
  id: PropTypes.string.isRequired,
  deletePost: PropTypes.func.isRequired
};

const actions = { deletePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(DeletePost);
