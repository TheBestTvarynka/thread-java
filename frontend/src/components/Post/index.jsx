import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import UserList from 'src/components/UsersList';
import PostMenu from 'src/components/PostMenu';

import styles from './styles.module.scss';

const Post = ({
  post,
  loadReactors,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleExpandedUpdatePost,
  sharePost,
  curUserId
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <UserList
          triggerTittle={likeCount.toString()}
          windowTitle="Users who likes your post"
          getter={() => loadReactors(id, true)}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
        </Label>
        <UserList
          triggerTittle={dislikeCount.toString()}
          windowTitle="Users who dislikes your post"
          getter={() => loadReactors(id, false)}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        <span className="right floated">
          {(curUserId === user.id) && (
            <PostMenu floated="right" id={id} toggleExpandedUpdatePost={toggleExpandedUpdatePost} />
          )}
        </span>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  loadReactors: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  curUserId: PropTypes.string.isRequired
};

export default Post;
