import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Icon, Image, Modal, Segment } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Spinner from 'src/components/Spinner';
import { toggleExpandedUpdatePost, updatePost } from '../../containers/Thread/actions';
import * as imageService from '../../services/imageService';
import styles from '../AddPost/styles.module.scss';

const UpdatePost = ({
  post,
  toggleExpandedUpdatePost: toggle,
  updatePost: update
}) => {
  const uploadImage = file => imageService.uploadImage(file);
  const [body, setBody] = useState(post.body);
  const img = post.image ? { imageId: post.image.id, imageLink: post.image.link } : undefined;
  const [image, setImage] = useState(img);
  const [id] = useState(post.id);
  const [isUploading, setIsUploading] = useState(false);

  const handleClose = () => {
    toggle();
  };

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await update({ imageId: image?.imageId, body, id });
    handleClose();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  const handleDeleteFile = async () => {
    setImage(undefined);
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={handleClose}>
      {post
        ? (
          <Modal.Content>
            <Segment>
              <Form onSubmit={handleUpdatePost}>
                <Form.TextArea
                  name="body"
                  value={body}
                  placeholder="What is the news?"
                  onChange={ev => setBody(ev.target.value)}
                />
                {image?.imageLink && (
                  <div className={styles.imageWrapper}>
                    <Image className={styles.image} src={image?.imageLink} alt="post" />
                  </div>
                )}
                <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                  <Icon name="image" />
                  Attach image
                  <input name="image" type="file" onChange={handleUploadFile} hidden />
                </Button>
                {image?.imageLink && (
                  <Button color="grey" size="small" onClick={handleDeleteFile}>
                    <Icon name="close" />
                    Delete image
                  </Button>
                )}
                <Button floated="right" color="blue" type="submit">Update</Button>
              </Form>
            </Segment>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

UpdatePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedUpdatePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedUpdatePost
});

const actions = { toggleExpandedUpdatePost, updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdatePost);
