package com.threadjava.post;

import com.threadjava.post.model.Post;
import com.threadjava.post.model.PostArchive;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PostArchiveRepository extends JpaRepository<PostArchive, UUID> {
    @Override
    PostArchive save(PostArchive entity);
}
