package com.threadjava.post;

import com.threadjava.comment.CommentService;
import com.threadjava.image.ImageService;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.postReactions.PostReactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private PostArchiveRepository postArchiveRepository;
    @Autowired
    private CommentService commentService;
    @Autowired
    private PostReactionService postReactionService;
    @Autowired
    private ImageService imageService;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public List<PostListDto> getAllForeignPosts(Integer from, Integer count, UUID userId) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllForeignPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public List<PostListDto> getAllLikedPosts(Integer from, Integer count, UUID userId) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllLikedPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentService.findAllCommentsDetailsByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentDetailsToPostCommentDto)
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        if (postDto.getId() != null) {
            var result = getPostById(postDto.getId());
            post.setCreatedAt(result.getCreatedAt());
        }
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public void deletePostById(UUID id) {
        var post = postsCrudRepository.findById(id);
        if (post.isPresent()) {
            archive(post.get());
            commentService.archiveAll(post.get().getComments());
            postReactionService.archiveAll(post.get().getReactions());
            var image = post.get().getImage();
            if (image != null) {
                imageService.archive(image);
            }
            postsCrudRepository.delete(post.get());
        }
    }

    public void archive(Post post) {
        var archivePost = PostMapper.MAPPER.postToPostArchive(post);
        postArchiveRepository.save(archivePost);
    }
}
