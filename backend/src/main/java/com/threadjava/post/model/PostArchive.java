package com.threadjava.post.model;

import com.threadjava.comment.model.Comment;
import com.threadjava.image.model.Image;
import com.threadjava.postReactions.model.PostReaction;
import com.threadjava.users.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@EqualsAndHashCode
@Table(name = "posts_archive")
public class PostArchive {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "deleted_on")
    @CreationTimestamp
    private Date deletedAt;

    @Column(name = "postId")
    private UUID postId;

    @Column(name = "body", columnDefinition="TEXT")
    private String body;

    @Column(name = "image_id")
    private UUID imageId;

    @Column(name = "user_id")
    private UUID user;
}
