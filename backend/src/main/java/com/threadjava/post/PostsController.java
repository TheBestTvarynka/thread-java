package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam() String filterType,
                                 @RequestParam(required = false) UUID param) {
        if (filterType.equals("ALL_POSTS")) {
            return postsService.getAllPosts(from, count, param);
        } else if (filterType.equals("OWN_POSTS")) {
            return postsService.getAllPosts(from, count, param);
        } else if (filterType.equals("FOREIGN_POSTS")) {
            return postsService.getAllForeignPosts(from, count, param);
        } else if (filterType.equals("LIKED_POSTS")) {
            return postsService.getAllLikedPosts(from, count, param);
        }
        return null;
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        postsService.deletePostById(id);
    }
}
