package com.threadjava.comment.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@EqualsAndHashCode
@Table(name = "comments_archive")
public class CommentArchive {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "deleted_on")
    @CreationTimestamp
    private Date deletedAt;

    @Column(name = "commentId")
    private UUID commentId;

    @Column(name = "`timestamp`")
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_on")
    @UpdateTimestamp
    private Date updatedAt;

    @Column(name = "body", columnDefinition="TEXT")
    private String body;

    @Column(name = "user_id")
    private UUID user;

    @Column(name = "post_id")
    private UUID post;
}
