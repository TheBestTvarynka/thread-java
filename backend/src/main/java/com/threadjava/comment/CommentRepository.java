package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {
    List<Comment> findAllByPostId(UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(" +
            "c.id," +
            "c.body, " +
            "c.user, " +
            "c.post, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c)" +
            ")" +
            "FROM Comment c " +
            "WHERE c.id = :id")
    Optional<CommentDetailsQueryResult> findCommentById(@Param("id") UUID id);

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(" +
            "c.id," +
            "c.body, " +
            "c.user, " +
            "c.post, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c)" +
            ")" +
            "FROM Comment c " +
            "WHERE c.post.id = :id")
    List<CommentDetailsQueryResult> findAllCommentsDetailsByPostId(@Param("id") UUID postId);
}
