package com.threadjava.comment;

import com.threadjava.comment.model.CommentArchive;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CommentArchiveRepository extends JpaRepository<CommentArchive, UUID> {
    @Override
    CommentArchive save(CommentArchive entity);
}
