package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.comment.model.CommentArchive;
import com.threadjava.commentReactions.CommentReactionService;
import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.commentReactions.model.CommentReactionArchive;
import com.threadjava.post.PostsRepository;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;
    @Autowired
    private CommentArchiveRepository commentArchiveRepository;
    @Autowired
    private CommentReactionService commentReactionService;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findById(id)
                .map(CommentMapper.MAPPER::commentToCommentDetailsDto)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }

    public void deleteCommentById(UUID id) {
        var comment = commentRepository.findById(id);
        if (comment.isPresent()) {
            var commentArchive = CommentMapper.MAPPER.commentToCommentArchive(comment.get());
            List<CommentReaction> commentReactions = comment.get().getReactions();
            commentReactionService.archiveAll(commentReactions);
            commentArchiveRepository.save(commentArchive);
            commentRepository.delete(comment.get());
        }
    }

    public void archiveAll(List<Comment> comments) {
        List<CommentArchive> commentArchives = comments.stream()
                .map(CommentMapper.MAPPER::commentToCommentArchive)
                .collect(Collectors.toList());
        List<List<CommentReaction>> commentsReactions = comments.stream()
                .map(comment -> comment.getReactions())
                .collect(Collectors.toList());
        commentsReactions.stream()
                .forEach(commentReactions -> commentReactionService.archiveAll(commentReactions));
        commentArchiveRepository.saveAll(commentArchives);
    }

    public List<Comment> findAllByPostId(UUID postId) {
        return commentRepository.findAllByPostId(postId);
    }

    public List<CommentDetailsDto> findAllCommentsDetailsByPostId(UUID postId) {
        return commentRepository.findAllCommentsDetailsByPostId(postId).stream()
                .map(comment -> CommentMapper.MAPPER.commentQueryResultToCommentDetails(comment))
                .collect(Collectors.toList());
    }
}
