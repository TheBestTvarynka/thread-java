package com.threadjava.comment.dto;

import com.threadjava.post.dto.PostUserDto;
import com.threadjava.post.model.Post;
import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDetailsQueryResult {
    private UUID id;
    private String body;
    private User user;
    private Post post;
    public long likeCount;
    public long dislikeCount;
}
