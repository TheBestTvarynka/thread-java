package com.threadjava.comment.dto;

import com.threadjava.post.dto.PostUserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDetailsDto {
    private UUID id;
    private String body;
    private PostUserDto user;
    private UUID postId;
    public long likeCount;
    public long dislikeCount;
}
