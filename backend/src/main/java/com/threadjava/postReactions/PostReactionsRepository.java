package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReactionUserQueryResult;
import com.threadjava.postReactions.model.PostReaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostReactionsRepository extends JpaRepository<PostReaction, UUID> {
    @Query("SELECT r " +
            "FROM PostReaction r " +
            "WHERE r.user.id = :userId AND r.post.id = :postId ")
    Optional<PostReaction> getPostReaction(@Param("userId") UUID userId, @Param("postId") UUID postId);

    List<PostReaction> findAllByPostId(UUID postId);

    @Query("SELECT new com.threadjava.postReactions.dto.ReactionUserQueryResult(" +
            "pr.user.id, " +
            "pr.user.username, " +
            "pr.user.avatar) " +
            "FROM PostReaction pr " +
            "WHERE pr.isLike = :isLiked AND pr.post.id = :postId")
    List<ReactionUserQueryResult> findAllReactors(@Param("postId") UUID postId, @Param("isLiked") Boolean isLiked);
}
