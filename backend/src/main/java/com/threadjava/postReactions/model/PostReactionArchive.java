package com.threadjava.postReactions.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@EqualsAndHashCode
@Table(name = "post_reactions_archive")
public class PostReactionArchive {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "deleted_on")
    @CreationTimestamp
    private Date deletedAt;

    @Column(name = "reactionId")
    private UUID reactionId;

    @Column(name = "`timestamp`")
    private Date createdAt;

    @Column(name = "updated_on")
    private Date updatedAt;

    @Column(name = "isLike")
    private Boolean isLike;

    @Column(name = "user_id")
    private UUID user;

    @Column(name = "post_id")
    private UUID post;
}
