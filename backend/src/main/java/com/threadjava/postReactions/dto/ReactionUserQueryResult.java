package com.threadjava.postReactions.dto;

import com.threadjava.image.model.Image;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReactionUserQueryResult {
    private UUID id;
    private String username;
    private Image avatar;
}
