package com.threadjava.postReactions;

import com.threadjava.post.dto.PostUserDto;
import com.threadjava.postReactions.dto.ReactionUserQueryResult;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.postReactions.model.PostReaction;
import com.threadjava.postReactions.model.PostReactionArchive;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PostReactionMapper {
    PostReactionMapper MAPPER = Mappers.getMapper( PostReactionMapper.class );

    @Mapping(source = "post.id", target = "postId")
    @Mapping(source = "user.id", target = "userId")
	@Mapping(target = "isNew", ignore = true)
    ResponsePostReactionDto reactionToPostReactionDto(PostReaction postReaction);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(source = "id", target = "reactionId")
    @Mapping(source = "post.id", target = "post")
    @Mapping(source = "user.id", target = "user")
    PostReactionArchive postReactionToPostReactionArchive(PostReaction postReaction);

    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "postId", target = "post.id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    PostReaction dtoToPostReaction(ReceivedPostReactionDto postReactionDto);

    @Mapping(source = "avatar", target = "image")
    PostUserDto reactionUserToPostUser(ReactionUserQueryResult user);
}
