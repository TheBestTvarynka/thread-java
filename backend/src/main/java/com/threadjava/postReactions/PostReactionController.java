package com.threadjava.postReactions;

import com.threadjava.post.dto.PostUserDto;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {
    @Autowired
    private PostReactionService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction){
        postReaction.setUserId(getUserId());
        var reaction = postsService.setReaction(postReaction);

        if (reaction.isPresent() && reaction.get().getUserId() != getUserId()) {
            // notify a user if someone (not himself) liked his post
            if (postReaction.getIsLike()) {
                template.convertAndSend("/topic/like", "Your post was liked!");
            } else {
                template.convertAndSend("/topic/dislike", "Your post was disliked!");
            }
        }
        return reaction;
    }

    @GetMapping("/likers/{id}")
    public List<PostUserDto> getPostLikers(@PathVariable UUID id) {
        return postsService.getReactors(id, true);
    }

    @GetMapping("/dislikers/{id}")
    public List<PostUserDto> getPostDislikers(@PathVariable UUID id) {
        return postsService.getReactors(id, false);
    }
}
