package com.threadjava.postReactions;

import com.threadjava.post.dto.PostUserDto;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.postReactions.model.PostReaction;
import com.threadjava.postReactions.model.PostReactionArchive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostReactionService {
    @Autowired
    private PostReactionsRepository postReactionsRepository;
    @Autowired
    private PostReactionsArchiveRepository postReactionsArchiveRepository;

    public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto postReactionDto) {

        var reaction = postReactionsRepository.getPostReaction(postReactionDto.getUserId(), postReactionDto.getPostId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == postReactionDto.getIsLike()) {
                postReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(postReactionDto.getIsLike());
                var result = postReactionsRepository.save(react);
                var response = PostReactionMapper.MAPPER.reactionToPostReactionDto(result);
                response.setIsNew(false);
                return Optional.of(response);
            }
        } else {
            var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDto);
            var result = postReactionsRepository.save(postReaction);
            var response = PostReactionMapper.MAPPER.reactionToPostReactionDto(result);
            response.setIsNew(true);
            return Optional.of(response);
        }
    }

    public void archiveAll(List<PostReaction> postReactions) {
        List<PostReactionArchive> reactionArchives = postReactions.stream()
                .map(PostReactionMapper.MAPPER::postReactionToPostReactionArchive)
                .collect(Collectors.toList());
        postReactionsArchiveRepository.saveAll(reactionArchives);
    }

    public List<PostUserDto> getReactors(UUID postId, Boolean isLiked) {
        List<PostUserDto> users = postReactionsRepository.findAllReactors(postId, isLiked).stream()
                .map(PostReactionMapper.MAPPER::reactionUserToPostUser)
                .collect(Collectors.toList());
        return users;
    }
}
