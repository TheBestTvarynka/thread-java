package com.threadjava.postReactions;

import com.threadjava.postReactions.model.PostReactionArchive;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PostReactionsArchiveRepository extends JpaRepository<PostReactionArchive, UUID> {
    @Override
    PostReactionArchive save(PostReactionArchive entity);
}
