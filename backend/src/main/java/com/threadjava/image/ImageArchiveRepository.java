package com.threadjava.image;

import com.threadjava.image.model.ImageArchive;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ImageArchiveRepository extends JpaRepository<ImageArchive, UUID> {
    @Override
    ImageArchive save(ImageArchive entity);
}
