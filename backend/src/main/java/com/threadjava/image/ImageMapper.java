package com.threadjava.image;

import com.threadjava.image.dto.ImageDto;
import com.threadjava.image.model.Image;
import com.threadjava.image.model.ImageArchive;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ImageMapper {
    ImageMapper MAPPER = Mappers.getMapper(ImageMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(source = "id", target = "imageId")
    ImageArchive imageToImageArchive(Image image);

    ImageDto imageToImageDto(Image image);
}
