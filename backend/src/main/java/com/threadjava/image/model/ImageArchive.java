package com.threadjava.image.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@EqualsAndHashCode
@Table(name = "images_archive")
public class ImageArchive {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "deleted_on")
    @CreationTimestamp
    private Date deletedAt;

    @Column(name = "imageId")
    private UUID imageId;

    @Column(name = "`timestamp`")
    private Date createdAt;

    @Column(name = "updated_on")
    private Date updatedAt;

    @Column(name = "link", nullable = false)
    private String link;

    @Column(name = "delete_hash", nullable = false)
    private String deleteHash;
}
