package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.commentReactions.model.CommentReactionArchive;
import com.threadjava.post.dto.PostUserDto;
import com.threadjava.postReactions.PostReactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentReactionService {
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;
    @Autowired
    private CommentReactionsArchiveRepository commentReactionsArchiveRepository;

    public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {
        var reaction = commentReactionsRepository.getCommentReaction(commentReactionDto.getUserId(), commentReactionDto.getCommentId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == commentReactionDto.getIsLike()) {
                commentReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(commentReactionDto.getIsLike());
                var result = commentReactionsRepository.save(react);
                var response = CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result);
                response.setIsNew(false);
                return Optional.of(response);
            }
        } else {
            var postReaction = CommentReactionMapper.MAPPER.dtoToPostReaction(commentReactionDto);
            var result = commentReactionsRepository.save(postReaction);
            var response = CommentReactionMapper.MAPPER.reactionToCommentReactionDto(result);
            response.setIsNew(true);
            return Optional.of(response);
        }
    }

    public void archiveAll(List<CommentReaction> postReactions) {
        List<CommentReactionArchive> reactionArchives = postReactions.stream()
                .map(CommentReactionMapper.MAPPER::commentReactionToCommentReactionArchive)
                .collect(Collectors.toList());
        commentReactionsArchiveRepository.saveAll(reactionArchives);
    }

    public List<PostUserDto> getReactors(UUID commentId, Boolean isLiked) {
        List<PostUserDto> users = commentReactionsRepository.findAllReactors(commentId, isLiked).stream()
                .map(PostReactionMapper.MAPPER::reactionUserToPostUser)
                .collect(Collectors.toList());
        return users;
    }
}
