package com.threadjava.commentReactions;

import com.threadjava.commentReactions.model.CommentReactionArchive;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CommentReactionsArchiveRepository extends JpaRepository<CommentReactionArchive, UUID> {
    @Override
    CommentReactionArchive save(CommentReactionArchive entity);
}
