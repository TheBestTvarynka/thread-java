package com.threadjava.commentReactions;

import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.postReactions.dto.ReactionUserQueryResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentReactionsRepository extends JpaRepository<CommentReaction, UUID> {
    @Query("SELECT r " +
            "FROM CommentReaction r " +
            "WHERE r.user.id = :userId AND r.comment.id = :commentId ")
    Optional<CommentReaction> getCommentReaction(@Param("userId") UUID userId, @Param("commentId") UUID commentId);

    List<CommentReaction> findAllByCommentId(UUID commentId);

    @Query("SELECT new com.threadjava.postReactions.dto.ReactionUserQueryResult(" +
            "cr.user.id, " +
            "cr.user.username, " +
            "cr.user.avatar) " +
            "FROM CommentReaction cr " +
            "WHERE cr.isLike = :isLiked AND cr.comment.id = :commentId")
    List<ReactionUserQueryResult> findAllReactors(@Param("commentId") UUID commentId, @Param("isLiked") Boolean isLiked);
}
