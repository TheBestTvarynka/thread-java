package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.post.dto.PostUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionController {
    @Autowired
    private CommentReactionService commentReactionService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReaction) {
        commentReaction.setUserId(getUserId());
        var reaction = commentReactionService.setReaction(commentReaction);

        if (reaction.isPresent() && reaction.get().getUserId() != getUserId()) {
            // notify a user if someone (not himself) liked his post
            if (commentReaction.getIsLike()) {
                template.convertAndSend("/topic/like", "Your comment was liked!");
            } else {
                template.convertAndSend("/topic/dislike", "Your comment was disliked!");
            }
        }
        return reaction;
    }

    @GetMapping("/likers/{id}")
    public List<PostUserDto> getPostLikers(@PathVariable UUID id) {
        return commentReactionService.getReactors(id, true);
    }

    @GetMapping("/dislikers/{id}")
    public List<PostUserDto> getPostDislikers(@PathVariable UUID id) {
        return commentReactionService.getReactors(id, false);
    }
}
